import 'package:flutter/material.dart';

import '../constants.dart';
import '../model/email.dart';
import '../model/zapato.dart';

class DetailScreen extends StatelessWidget {
 
  final Zapato zapato;

  const DetailScreen({Key? key, required this.zapato}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(zapato.nombre),
        ),
        body: Container(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('TALLA: ${zapato.talla}', style: fromTextStyle),
              const SizedBox(height: 10.0),
              const Divider(color: primaryColor),
              const SizedBox(height: 10.0),
              Text(zapato.precio, style: subjectTextStyle),
              const SizedBox(height: 5.0),
              Text('PRECIO: ${zapato.precio}', style: dateTextStyle),
              const SizedBox(height: 10.0),
              const Divider(color: primaryColor),
              const SizedBox(height: 10.0),
              Text('MODELO: ${zapato.modelo} ', style: bodyTextStyle),
                ElevatedButton(
          style: ElevatedButton.styleFrom(
          primary: Color.fromARGB(184, 81, 21, 149), // background
          onPrimary: Colors.white, // foreground
        ),
        onPressed: () { 
          Navigator.pop(context);
        },
        child: Text('Done'),
)
            ],
          ),
        ));
  }
}
